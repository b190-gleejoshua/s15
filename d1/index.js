// alert("Hello World!");

// console.log allows the browser to display the message that is inside the browser
console.log("Hello World!")
console.
log
(
	"Hello Again"
);

// Variables
// it is used to store data 
// any information that is used by an application is stored in what we called "memory";
// when we create variables, certain portions of a device's memory is given "name" that we call variables

// if the variable has been called without declaring the variable itself, the console would render that the variable that we are calling is "not defined"
 
 // Declaring a varable
 // variables are initialized witht the use of let/cons keyword

// after declaring the variable(declaring a variable means that we have created a variable and it os now ready to receive data), failure to assign a value to it means that the variable is "undefined"


 let myVariable="Hello";


console.log(myVariable);
/*
	Guides in writing variables
		-using the right keyword is a way for a dev to succsessfully initialize a variable(let/cons)
		-variable names should start with lowercase character, and camelCasing for multiple words
		-variable name should be indicative or descriptive of the value being stored
*/

// let firstName="Glee";
// let pokemon=2500;a bad naming since we are confused as to what 2500 gonna do with pokemon

// let FirstName="Glee";/*not following the camelCasing*/
// let firstName="Glee";

// we can use underscores for naming variables

// such examples of camelCasings are:
	// lastName emailAddress mobileNumber


// Declaring and initializing variables
// default of there's no keyword is let value
let productName="Desktop Computer";
console.log(productName);


// create a productPrice variable with the value 18999
let productPrice=18999;
console.log(productPrice);


// In the context of certain applications, some variables/information are constant and should not be changed
// One example in real-world scenario is the interest for loan, savings account must not be changed due to implications in computation

const interest=3.539
console.log(interest)


// Reassigning of variable values
// reassigning a variable means that we are going to change the initial or previous value 	into another value

// we can change the value of a let variable

/*
	SYNTAX:
		letVariableName = newValue;
*/

productName="Laptop";
console.log(productName);

 let friend="Luffy";
console.log(friend);

friend="Zoro";	
console.log(friend);


// cons variable values cannot and should not be changed by the devs
// if we declare a variable using const, we can neither update nor the variable value cannot be reassigned
/*interest=4.489
console.log(interest)*/

/*
	when to use JS const for a variable
		as a general rule, always declare a variable with const unless the value will change
*/

// Reassigning vs initializing
let supplier;
// this is technically an initialization since we are assigning a value to the variable the first time
supplier="John Smith Tradings";
console.log(supplier);

// This is reassigning since the value has been reassign
supplier="Zuitt Store";
console.log(supplier)


// var vs let/const

// some of you may wonder why we use let/const when we search online, we see var

// var is also used in declaring a variable. but var is an ECMAScript1(ES1) feature[ES1(Javascript 1997)]
// let/const - introduced as new features of ES6(2015)

// difference

// there are issues when it comes to variables declared using var, regarding hoisting
// in terms of variables,keyword var is hoisted while let/const does not allow hoisting
// Hoisting is Javascript's default behavior of moving declrations to the top
a=5;
console.log(a);
var a;
a=6
console.log(a)

// scope of variables
/*
	-scope essentially means where these variables are available for use
	-let and const variables are block scoop
	-a block is a chunk of codes bounded by {}. a block lives in curly braces. anything within {} is a block
*/

// let outerVariable="hello";
// {
// 	let innerVariable="hello again";
// 	console.log(innerVariable)
// }

// console.log(outerVariable)
// // console.log(innerVariable)

// Multiple varible declaration

/*
	multiple varibles can be declared in one line using one keyword
	the two variable declaration must be separated by a comma
	should the second variable not be changed, use separate declarations
		let productCode="CD017"
		const productBrand="Del"
*/

let productCode="CD017", productBrand="Del";

console.log(productCode)
console.log(productBrand)

// trying to use a variable with a reserved keyword
/*const let="hello";
console.log(let);
*/
// Data Types in Javascript
// Strings are series of characters that creates a word, a phrase or anything related to creating a text


let country="Philippines";
console.log(country);

// Concatenating strings
// we are combining multiple values/variables with string values with the "+" symbol
let province="Metro Manila";
console.log(province +","+country)

// escape character


let mailAddress="Metro Manila\n\nPhilippines"
console.log(mailAddress)



// Numbers
// Intergers/whole numbers
// with the exception if strings, other data types in javascript are color coded

let headcount = 26;
console.log(headcount);

// decimal/fractions
let grade=98.7;
console.log(grade);


// exponent notation
let planetDistance=2e10;
console.log(planetDistance);

// combining strings and numbers
// when numbers and strings are combined, the resulting data type would be a string
console.log("John's grade last qaurter is " + grade)

// Boolean
// are normally used to store values relating to the state of certain things
// true/false for the default value
let isMarried = false;
let inGoodConduct=true;

console.log("isMarried: " +isMarried);
console.log("inGoodConduct: "+inGoodConduct);

// Array
// are special kinds of data type that are used to store multiple values
// can also store data types but it is normally used to store similar data types



/*
	SYNTAX:
	let/const varName = [elementA, elementB, element

*/
// similar data type
let grades = [98.7, 96., 94.5,87.9];
console.log(grades);


// different data type
// it is not advisable to use different data types in an array since it is confusing to other devs when they read our codes
let person=["John","Smith", 32, true];
console.log(person);


// object data type
// objects are another special kind of data type that's used to mimic the real world object
// they are used to create complex data that contains pieces of information that are relevant to each other

/*
	SYNTAX
		let/const varName = {
			propertyA: value
			propertyB: value
		}

*/

let personDetails={
	fullName:"Juan Dela Cruz",
	age:35,
	isMarried:true,
	contacts:["091235483", "0998564327"],
	address:{
		houseNumber:"345",
		city:"Manila"
	}
};

console.log(personDetails);


// typeof keyword - used to determine the data type of a variable
console.log(typeof personDetails)

/*
	Constant Array/Objects
		the const keyword is a bit misleading when it comes to arrays/object

		it does not define a constant value for arrays/objects. it defines a constant reference to a value

		we CANNOT
		Reassign a constant value
		Reassign a constant array
		Reassign a constant object


		but you CAN
		change the elements of a constant array,
		change the properties of a constant object
*/

const anime = ["Naruto", "Slam Dunk", "One Piece"];
console.log(anime);

// anime =["Akame ga kill"];
anime[0] =["Akame ga kill"];
console.log(anime);

// Null data type
let number = 0;
let string = "";

console.log(number);
console.log(string);
let jowa = null;
console.log(jowa);